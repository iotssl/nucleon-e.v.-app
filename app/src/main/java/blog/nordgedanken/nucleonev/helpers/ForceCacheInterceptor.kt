package blog.nordgedanken.nucleonev.helpers

import android.content.Context
import okhttp3.Interceptor
import okhttp3.CacheControl
import okhttp3.Response
import java.io.IOException
import android.net.ConnectivityManager




/**
 * Created by MTRNord on 06.10.2018.
 */
class ForceCacheInterceptor(val context: Context) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        if (!isNetworkAvailable()) {
            builder.cacheControl(CacheControl.FORCE_CACHE)
        }

        return chain.proceed(builder.build())
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}