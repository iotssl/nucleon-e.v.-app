package blog.nordgedanken.nucleonev.ttn.models.api.application

import com.google.gson.annotations.SerializedName

data class GetDevice(

	@field:SerializedName("altitude")
	val altitude: Int? = null,

	@field:SerializedName("lorawan_device")
	val lorawanDevice: LorawanDevice? = null,

	@field:SerializedName("latitude")
	val latitude: Double? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("attributes")
	val attributes: Attributes? = null,

	@field:SerializedName("app_id")
	val appId: String? = null,

	@field:SerializedName("dev_id")
	val devId: String? = null,

	@field:SerializedName("longitude")
	val longitude: Double? = null
)