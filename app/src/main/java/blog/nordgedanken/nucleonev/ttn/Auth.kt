package blog.nordgedanken.nucleonev.ttn

import android.bluetooth.BluetoothClass
import blog.nordgedanken.nucleonev.ttn.models.TtnApplication

/**
 * Created by MTRNord on 01.01.2019.
 */
object Auth {
    var secretState: String? = null
    var code: String? = null
    var accessToken: String? = null

    var ttnApplications: List<TtnApplication>? = null
    var selectedApplication: TtnApplication? = null
    var selectedDevice: BluetoothClass.Device? = null

    val redirectURI = "https://nucleon-ev.eu/authed"
    // https://account.thethingsnetwork.org/users/authorize?response_type=code&client_id=nucleon_nodes&redirect_uri=https://nucleon-ev.eu/authed
    val authorizationUrl = "https://account.thethingsnetwork.org/users/authorize\n" +
            "?response_type=code\n" +
            "&client_id=nucleon_nodes\n" +
            "&redirect_uri=" + redirectURI

}