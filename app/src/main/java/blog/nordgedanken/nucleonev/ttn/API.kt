package blog.nordgedanken.nucleonev.ttn

import blog.nordgedanken.nucleonev.ttn.models.api.application.GetApplication
import blog.nordgedanken.nucleonev.ttn.models.api.application.GetDevice
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by MTRNord on 01.01.2019.
 */
object TTNApi {
    enum class Region(val id: String) {
        EU("eu"),
        USWEST("us-west"),
        ASIASE("asia-se"),
        BRAZIL("brazil")
    }

    var region: Region? = null
    private val retrofit: Retrofit by lazy { Retrofit.Builder()
            .baseUrl("http://${region?.id}.thethings.network:8084")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val app_service = retrofit.create(TTNApplicationService::class.java)
}


interface TTNApplicationService {
    @GET("/applications/{app_id}")
    fun getApplication(@Path("app_id") appID: String): Call<GetApplication>

    @POST("/applications/{app_id}")
    fun addApplication(@Path("app_id") appID: String, @Body application: GetApplication)

    @GET("/applications/{app_id}/devices/{dev_id}")
    fun getDevice(@Path("app_id") appID: String, @Path("dev_id") devID: String): Call<GetDevice>


    @POST("/applications/{app_id}/devices/{dev_id}")
    fun addDevice(@Path("app_id") appID: String, @Body device: GetDevice)
}