package blog.nordgedanken.nucleonev.ttn.models.api.application

import com.google.gson.annotations.SerializedName

data class LorawanDevice(

	@field:SerializedName("last_seen")
	val lastSeen: Int? = null,

	@field:SerializedName("uses32_bit_f_cnt")
	val uses32BitFCnt: Boolean? = null,

	@field:SerializedName("app_eui")
	val appEui: String? = null,

	@field:SerializedName("dev_id")
	val devId: String? = null,

	@field:SerializedName("f_cnt_up")
	val fCntUp: Int? = null,

	@field:SerializedName("disable_f_cnt_check")
	val disableFCntCheck: Boolean? = null,

	@field:SerializedName("app_key")
	val appKey: String? = null,

	@field:SerializedName("dev_addr")
	val devAddr: String? = null,

	@field:SerializedName("activation_constraints")
	val activationConstraints: String? = null,

	@field:SerializedName("f_cnt_down")
	val fCntDown: Int? = null,

	@field:SerializedName("nwk_s_key")
	val nwkSKey: String? = null,

	@field:SerializedName("dev_eui")
	val devEui: String? = null,

	@field:SerializedName("app_s_key")
	val appSKey: String? = null,

	@field:SerializedName("app_id")
	val appId: String? = null
)