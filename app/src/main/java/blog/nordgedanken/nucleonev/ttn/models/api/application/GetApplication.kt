package blog.nordgedanken.nucleonev.ttn.models.api.application

import com.google.gson.annotations.SerializedName

data class GetApplication(

	@field:SerializedName("register_on_join_access_key")
	val registerOnJoinAccessKey: String? = null,

	@field:SerializedName("payload_format")
	val payloadFormat: String? = null,

	@field:SerializedName("converter")
	val converter: String? = null,

	@field:SerializedName("validator")
	val validator: String? = null,

	@field:SerializedName("decoder")
	val decoder: String? = null,

	@field:SerializedName("app_id")
	val appId: String? = null,

	@field:SerializedName("encoder")
	val encoder: String? = null
)