package blog.nordgedanken.nucleonev.ttn.models.api.application

import com.google.gson.annotations.SerializedName

data class Attributes(

	@field:SerializedName("value")
	val value: String? = null,

	@field:SerializedName("key")
	val key: String? = null
)