package blog.nordgedanken.nucleonev

import android.app.Application
import com.mapbox.mapboxsdk.Mapbox
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger



/**
 * Created by MTRNord on 05.10.2018.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Logger.addLogAdapter(AndroidLogAdapter())

        // Mapbox Access token
        Mapbox.getInstance(applicationContext, getString(R.string.mapbox_access_token))
    }
}
