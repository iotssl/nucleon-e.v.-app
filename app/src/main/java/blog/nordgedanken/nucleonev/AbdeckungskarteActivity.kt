package blog.nordgedanken.nucleonev

import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import blog.nordgedanken.nucleonev.helpers.ForceCacheInterceptor
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.annotations.PolygonOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.style.layers.CircleLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_abdeckungskarte.*
import kotlinx.android.synthetic.main.app_bar_abdeckungskarte.*
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.doAsync
import java.net.URL


class AbdeckungskarteActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var mapView: MapView? = null
    private var cacheSize = 10 * 1024 * 1024 // 10MB
    private var client: OkHttpClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abdeckungskarte)
        setSupportActionBar(toolbar)

        client = OkHttpClient.Builder().cache(Cache(this@AbdeckungskarteActivity.cacheDir, cacheSize.toLong())).addInterceptor(ForceCacheInterceptor(this@AbdeckungskarteActivity)).build()

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        mapView = findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        initMap()

    }

    private fun initMap() {
        mapView?.getMapAsync { map ->
            // Setup boundingbox default view
            setupView(map)

            // Get various data Layers
            val existingGWURL = URL("https://riot.nordgedanken.de/iotAbdeckung/assets/geojson/sh-iotGW.json")
            val plannedTowerGWURL = URL("https://riot.nordgedanken.de/iotAbdeckung/assets/geojson/sh-iotGW-plannedTower.json")
            addGeoJsonCircleLayer(map, "IoTGateways-SH", existingGWURL)
            addGeoJsonCircleLayer(map, "Geplante IoTGateways-SH auf Fernmeldetürmen", plannedTowerGWURL)

            // Draw Circles around points
            doAsync {
                // existingGWs
                makeCircles(map, existingGWURL, 5000.0, Color.parseColor("#267e5c"))
                makeCircles(map, existingGWURL, 10000.0, Color.parseColor("#29AB87"))

                // plannedTowerGWs
                makeCircles(map, plannedTowerGWURL, 5000.0, Color.parseColor("#2a3f97"))
                makeCircles(map, plannedTowerGWURL, 10000.0, Color.parseColor("#3955ec"))

            }
        }
    }

    private fun addGeoJsonCircleLayer(map: MapboxMap, id: String, url: URL) {
        val source = GeoJsonSource(id, url)
        map.addSource(source)

        val layer = CircleLayer(id, id)
        layer.setProperties(
                PropertyFactory.visibility(Property.VISIBLE)
        )
        map.addLayer(layer)
    }

    private fun setupView(map: MapboxMap) {
        val latLngBounds = LatLngBounds.Builder()
                .include(LatLng(53.3471340781, 8.2720783721))
                .include(LatLng(53.3471340781, 11.3169272906))
                .include(LatLng(55.0612366435, 11.3169272906))
                .include(LatLng(55.0612366435, 8.2720783721))
                .include(LatLng(53.3471340781, 8.2720783721))
                .build()

        map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 1))
    }

    private fun makeCircles(map: MapboxMap, existingGWURL: URL, radius: Double, color: Int) {
        val request = Request.Builder()
                .url(existingGWURL)
                .build()
        val response = client?.newCall(request)?.execute()
        val featuresCollS = response?.body()?.string()
        val featuresColl = FeatureCollection.fromJson(featuresCollS!!)
        val features = featuresColl.features()
        features?.forEach {
            val geometry = it.geometry()
            when (geometry) {
                is Point -> {
                    val circle = polygonCircleForCoordinate(LatLng(geometry.latitude(), geometry.longitude()), radius)
                    runOnUiThread {
                        map.addPolygon(PolygonOptions().addAll(circle).fillColor(color).alpha(0.25F))
                    }
                }
                else -> Logger.d("WTF")
            }
        }
    }

    private fun polygonCircleForCoordinate(location: LatLng, radius: Double): ArrayList<LatLng> {
        val degreesBetweenPoints = 8 //45 sides
        val numberOfPoints = Math.floor((360 / degreesBetweenPoints).toDouble()).toInt()
        val distRadians = radius / 6371000.0 // earth radius in meters
        val centerLatRadians = location.latitude * Math.PI / 180
        val centerLonRadians = location.longitude * Math.PI / 180
        val polygons = ArrayList<LatLng>() //array to hold all the points
        for (index in 0 until numberOfPoints) {
            val degrees = (index * degreesBetweenPoints).toDouble()
            val degreeRadians = degrees * Math.PI / 180
            val pointLatRadians = Math.asin(Math.sin(centerLatRadians) * Math.cos(distRadians) + Math.cos(centerLatRadians) * Math.sin(distRadians) * Math.cos(degreeRadians))
            val pointLonRadians = centerLonRadians + Math.atan2(Math.sin(degreeRadians) * Math.sin(distRadians) * Math.cos(centerLatRadians),
                    Math.cos(distRadians) - Math.sin(centerLatRadians) * Math.sin(pointLatRadians))
            val pointLat = pointLatRadians * 180 / Math.PI
            val pointLon = pointLonRadians * 180 / Math.PI
            val point = LatLng(pointLat, pointLon)
            polygons.add(point)
        }
        return polygons
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.abdeckungskarte, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        mapView?.onSaveInstanceState(outState!!)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }
}
